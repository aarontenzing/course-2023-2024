How many airlines inside?
```
MATCH (airline)-[:type]->(a:Airline)
RETURN count(airline);
```

How many aircrafts inside? how many are active? Inactive?
```
    MATCH (aircraft)-[:type]->(a:Aircraft)
    WITH aircraft.hasStatus AS status
    RETURN status, COUNT(CASE WHEN status = "Active" THEN 1 END) AS activeCount, COUNT(CASE WHEN status = "Inactive" THEN 1 END) AS inactiveCount
```

Return the number of active aircrafts from each airline.
```
    MATCH (node)-[:Owns]->(aircraft)-[:type]->(a:Aircraft)
    WHERE aircraft.hasStatus = "Active"
    RETURN node, count(aircraft) as numberOfActive
```

How many aircrafts were created before 2010?
```
    MATCH (aircraft)-[:type]->(:Aircraft)
    WHERE toInteger(SPLIT(aircraft.hasCreationDate, "_")[0]) < 2010
    RETURN COUNT(aircraft);
```

How many aircrafts were modified? how many were modified in the second year of its creation?
```
    MATCH (aircraft)-[:type]->(:Aircraft)
    WHERE aircraft.hasModificationDate IS NOT NULL
    WITH aircraft,
        toInteger(SPLIT(aircraft.hasCreationDate, "_")[0]) AS creationYear,
        toInteger(SPLIT(aircraft.hasModificationDate, "_")[0]) AS modificationYear
    RETURN
    COUNT(aircraft) AS numberOfModifiedAircraft,
    COUNT(CASE WHEN modificationYear = creationYear + 1 THEN 1 END) AS numberOfModifiedInSecondYear;
```

How many aircraft model existed in total?
```
    MATCH (model)-[:type]->(:AircraftModel)
    RETURN COUNT(model);
```

Return all the aircraft models that have more than 5 versions.
```
    MATCH (version)<-[:hasModelVersion]-(model)-[:type]->(:AircraftModel)
    WITH model, count(version) AS countVersions
    WHERE countVersions >=5 
    RETURN model, countVersions;
```

Return all the companies that manufacture these aircrafts.
```
    MATCH (model)-[:type]->(:AircraftModel)
    WHERE (model.isManufacturedBy) is not null
    RETURN DISTINCT model.isManufacturedBy AS company;
```

How many aircraft models lack labels indicating their manufacturing companies?
```
    MATCH (model)-[:type]->(:AircraftModel)
    WHERE (model.isManufacturedBy) is null
    RETURN count(model);
```

Get all the properties for all the instances of all classes. (Property keys)
```
    MATCH (node)
    RETURN DISTINCT labels(node) AS labels, keys(node) AS properties;
```
