# Task b queries
1. Get all classes
```
SELECT DISTINCT ?class WHERE {
    ?class a owl:Ontology.
}
```
2. Get all the applicable properties for instances of all classes
```
SELECT DISTINCT ?property
WHERE {
  ?property a rdf:Property.
} 
```
3. Get all the properties, except for rdf:type, applicable to instances of the class
```
SELECT DISTINCT ?property
WHERE {
  ?property a rdf:Property.
  filter (?property != rdf:type)
} 
```
4. How many different values can you find for the whole set of properties (except for rdf:type) applicable to instances of the class (choose one class you like)?
```
SELECT DISTINCT ?property
WHERE {
  ?s rdf:type ontology:Aircraft.
  ?s ?p ?o.
  filter (?p != rdf:type)
} 
```
5. How many different values can you find for each of the properties (except for
rdf:type) applicable to instances of the class (that you just choose)?
```
SELECT ?property
WHERE {
  ?s rdf:type ontology:Aircraft.
  ?s ?p ?o.
  filter (?p != rdf:type)
} 
```
6. For all the properties, except for rdf:type, applicable to the instances of the class
you choose, tell me the average number of distinct values that they take on those
instances
```
SELECT ?p (avg(?ct) as ?avg)
WHERE {
    SELECT ?s ?p (count(distinct ?o) as ?ct)
    WHERE {
        ?s rdf:type ontology:Aircraft.
        ?s ?p ?o
        filter (?p != rdf:type)
    }GROUP BY ?s ?p
}GROUP BY ?p 
```
7. For all of the properties, except for rdf:type, applicable to the instances of the class
you choose, ordered number of distinct values that they take among all those
instances in a decreasing order (in other words, which are the most common
properties among the instances of the class?)
```
SELECT ?property (count(distinct ?value) as ?count)
    WHERE {
        ?instance rdf:type ontology:Aircraft.
        ?instance ?property ?value.
        filter (?property != rdf:type)
    }
    group by ?property
    order by desc(?count)
```