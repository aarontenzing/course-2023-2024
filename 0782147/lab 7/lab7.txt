1) MATCH (n)-[:hasOwningAirline]->(obj) return count(distinct obj);
2A) MATCH (n)-[:type]->(:Aircraft) return count(DISTINCT n), n;
2B) MATCH (n)-[:type]->(:Aircraft) where n.hasStatus= "Active" return count(DISTINCT n), n;
2C) MATCH (n)-[:type]->(:Aircraft) where n.hasStatus= "Inactive" return count(DISTINCT n), n;
3) MATCH (n)-[:Owns]-(obj)-[:type]->(:Aircraft) where obj.hasStatus= "Active" return count(obj), n;
4) MATCH (n)-[:type]->(:Aircraft) where toInteger(substring(n.hasCreationDate, 0, 4)) < 2010  return  n;
5A) MATCH (n)-[:type]->(:Aircraft) where n.hasModificationDate is not null return count(n);
5B) MATCH (n)-[:type]->(:Aircraft) where n.hasModificationDate is not null and toInteger(substring(n.hasModificationDate, 0, 4)) = toInteger(substring(n.hasCreationDate, 0, 4))+1  return count(n);
6) MATCH (:AircraftModel)-[]-(n) return count(DISTINCT n)
7) MATCH (n)-[:hasModelVersion]->(obj) WITH n, count(n) as modelcount where modelcount > 5 RETURN n;
8) MATCH (:AircraftModel)-[]-(n) return distinct n.hasModelName, n.isManufacturedBy;
9) MATCH (:AircraftModel)-[]-(n) WITH n.isManufacturedBy as manufacturer WHERE manufacturer is NULL return count(*);
10) MATCH (n) WITH keys(n) AS allProperties UNWIND allProperties AS property RETURN DISTINCT property
