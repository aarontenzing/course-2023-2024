##### Changes #####
- I added the shapes AirportActivityShape and LandingActivityShape
- I changed sh:lessThan to sh:lessThanOrEquals (since that constraint is more correct)
- Added the output_ld.ttl file which contains the triples (that I generated previous lab) that I used to validate it 
- Added propery p_has_Aircraft_Model to AircraftShape (since it is defined in the ontology, but was missing in the AircraftShape)