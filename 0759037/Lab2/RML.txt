@prefix rr: <http://www.w3.org/ns/r2rml#> .
@prefix rml: <http://semweb.mmlab.be/ns/rml#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix ql: <http://semweb.mmlab.be/ns/ql#> .
@prefix map: <http://mapping.example.com/> .
@prefix ma: <http://www.w3.org/ns/ma-ont#> .
@prefix schema: <http://schema.org/> .
@prefix ex: <http://www.example.com/> .
@prefix grel: <http://users.ugent.be/~bjdmeest/function/grel.ttl#> .

map:map_aircraft_000 rml:logicalSource map:source_000 ;
	rdf:type rr:TriplesMap ;
	rdfs:label "aircraft" ;
	rr:predicateObjectMap map:pom_000, map:pom_001, map:pom_002, map:pom_003, map:pom_004, map:pom_005, map:pom_006, map:pom_007, map:pom_008, map:pom_009, map:pom_010, map:pom_011, map:pom_012 ;
	rr:subjectMap map:s_000 .

map:map_aircraft_001 rml:logicalSource map:source_001 ;
	rdf:type rr:TriplesMap ;
	rdfs:label "aircraft" ;
	rr:predicateObjectMap map:pom_013, map:pom_014, map:pom_015, map:pom_016, map:pom_017, map:pom_018, map:pom_019, map:pom_020, map:pom_021, map:pom_022, map:pom_023, map:pom_024, map:pom_025 ;
	rr:subjectMap map:s_001 .

map:map_geo_000 rml:logicalSource map:source_002 ;
	rdf:type rr:TriplesMap ;
	rdfs:label "geo" ;
	rr:predicateObjectMap map:pom_026, map:pom_027 ;
	rr:subjectMap map:s_002 .

map:map_operatingairline_000 rml:logicalSource map:source_003 ;
	rdf:type rr:TriplesMap ;
	rdfs:label "operatingairline" ;
	rr:predicateObjectMap map:pom_028, map:pom_029 ;
	rr:subjectMap map:s_003 .

map:map_publishedairline_000 rml:logicalSource map:source_004 ;
	rdf:type rr:TriplesMap ;
	rdfs:label "publishedairline" ;
	rr:predicateObjectMap map:pom_030 ;
	rr:subjectMap map:s_004 .

map:om_000 rdf:type rr:ObjectMap ;
	rr:constant schema:Aircraft ;
	rr:termType rr:IRI .

map:om_001 rml:reference "aircraft_model" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_002 rml:reference "airline" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_003 rml:reference "creation_date" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_004 rml:reference "modification_date" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_005 rml:reference "status" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_006 rml:reference "tail_number" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_007 rml:reference "aircraft_body_type" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_008 rml:reference "aircraft_manufacturer" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_009 rml:reference "aircraft_version" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_010 rml:reference "landing_aircraft_type" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_011 rml:reference "landing_count" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_012 rml:reference "total_landed_weight" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_013 rdf:type rr:ObjectMap ;
	rr:constant schema:Aircraft ;
	rr:termType rr:IRI .

map:om_014 rml:reference "aircraft_model" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_015 rml:reference "airline" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_016 rml:reference "creation_date" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_017 rml:reference "modification_date" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_018 rml:reference "status" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_019 rml:reference "tail_number" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_020 rml:reference "aircraft_body_type" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_021 rml:reference "aircraft_manufacturer" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_022 rml:reference "aircraft_version" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_023 rml:reference "landing_aircraft_type" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_024 rml:reference "landing_count" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_025 rml:reference "total_landed_weight" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_026 rdf:type rr:ObjectMap ;
	rr:constant schema:Geo ;
	rr:termType rr:IRI .

map:om_027 rml:reference "geo_summary" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_028 rdf:type rr:ObjectMap ;
	rr:constant schema:Airline ;
	rr:termType rr:IRI .

map:om_029 rml:reference "operating_airline" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:om_030 rml:reference "published_airline" ;
	rdf:type rr:ObjectMap ;
	rr:termType rr:Literal .

map:pm_000 rdf:type rr:PredicateMap ;
	rr:constant rdf:type .

map:pm_001 rdf:type rr:PredicateMap ;
	rr:constant schema:HasModel .

map:pm_002 rdf:type rr:PredicateMap ;
	rr:constant schema:HasAirline .

map:pm_003 rdf:type rr:PredicateMap ;
	rr:constant schema:HasCreationDate .

map:pm_004 rdf:type rr:PredicateMap ;
	rr:constant schema:HasModificationDate .

map:pm_005 rdf:type rr:PredicateMap ;
	rr:constant schema:HasStatus .

map:pm_006 rdf:type rr:PredicateMap ;
	rr:constant schema:HasTailNumber .

map:pm_007 rdf:type rr:PredicateMap ;
	rr:constant schema:HasBodyType .

map:pm_008 rdf:type rr:PredicateMap ;
	rr:constant schema:HasManufacturer .

map:pm_009 rdf:type rr:PredicateMap ;
	rr:constant schema:HasVersion .

map:pm_010 rdf:type rr:PredicateMap ;
	rr:constant schema:HasLadingAircraftType .

map:pm_011 rdf:type rr:PredicateMap ;
	rr:constant schema:HasLandingCount .

map:pm_012 rdf:type rr:PredicateMap ;
	rr:constant schema:HasTotalWeight .

map:pm_013 rdf:type rr:PredicateMap ;
	rr:constant rdf:type .

map:pm_014 rdf:type rr:PredicateMap ;
	rr:constant schema:HasModel .

map:pm_015 rdf:type rr:PredicateMap ;
	rr:constant schema:HasAirline .

map:pm_016 rdf:type rr:PredicateMap ;
	rr:constant schema:HasCreationDate .

map:pm_017 rdf:type rr:PredicateMap ;
	rr:constant schema:HasModificationDate .

map:pm_018 rdf:type rr:PredicateMap ;
	rr:constant schema:HasStatus .

map:pm_019 rdf:type rr:PredicateMap ;
	rr:constant schema:HasTailNumber .

map:pm_020 rdf:type rr:PredicateMap ;
	rr:constant schema:HasBodyType .

map:pm_021 rdf:type rr:PredicateMap ;
	rr:constant schema:HasManufacturer .

map:pm_022 rdf:type rr:PredicateMap ;
	rr:constant schema:HasVersion .

map:pm_023 rdf:type rr:PredicateMap ;
	rr:constant schema:HasLadingAircraftType .

map:pm_024 rdf:type rr:PredicateMap ;
	rr:constant schema:HasLandingCount .

map:pm_025 rdf:type rr:PredicateMap ;
	rr:constant schema:HasTotalWeight .

map:pm_026 rdf:type rr:PredicateMap ;
	rr:constant rdf:type .

map:pm_027 rdf:type rr:PredicateMap ;
	rr:constant schema:geo_summary .

map:pm_028 rdf:type rr:PredicateMap ;
	rr:constant rdf:type .

map:pm_029 rdf:type rr:PredicateMap ;
	rr:constant schema:HasOperating .

map:pm_030 rdf:type rr:PredicateMap ;
	rr:constant schema:HasPublished .

map:pom_000 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_000 ;
	rr:predicateMap map:pm_000 .

map:pom_001 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_001 ;
	rr:predicateMap map:pm_001 .

map:pom_002 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_002 ;
	rr:predicateMap map:pm_002 .

map:pom_003 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_003 ;
	rr:predicateMap map:pm_003 .

map:pom_004 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_004 ;
	rr:predicateMap map:pm_004 .

map:pom_005 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_005 ;
	rr:predicateMap map:pm_005 .

map:pom_006 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_006 ;
	rr:predicateMap map:pm_006 .

map:pom_007 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_007 ;
	rr:predicateMap map:pm_007 .

map:pom_008 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_008 ;
	rr:predicateMap map:pm_008 .

map:pom_009 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_009 ;
	rr:predicateMap map:pm_009 .

map:pom_010 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_010 ;
	rr:predicateMap map:pm_010 .

map:pom_011 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_011 ;
	rr:predicateMap map:pm_011 .

map:pom_012 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_012 ;
	rr:predicateMap map:pm_012 .

map:pom_013 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_013 ;
	rr:predicateMap map:pm_013 .

map:pom_014 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_014 ;
	rr:predicateMap map:pm_014 .

map:pom_015 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_015 ;
	rr:predicateMap map:pm_015 .

map:pom_016 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_016 ;
	rr:predicateMap map:pm_016 .

map:pom_017 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_017 ;
	rr:predicateMap map:pm_017 .

map:pom_018 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_018 ;
	rr:predicateMap map:pm_018 .

map:pom_019 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_019 ;
	rr:predicateMap map:pm_019 .

map:pom_020 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_020 ;
	rr:predicateMap map:pm_020 .

map:pom_021 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_021 ;
	rr:predicateMap map:pm_021 .

map:pom_022 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_022 ;
	rr:predicateMap map:pm_022 .

map:pom_023 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_023 ;
	rr:predicateMap map:pm_023 .

map:pom_024 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_024 ;
	rr:predicateMap map:pm_024 .

map:pom_025 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_025 ;
	rr:predicateMap map:pm_025 .

map:pom_026 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_026 ;
	rr:predicateMap map:pm_026 .

map:pom_027 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_027 ;
	rr:predicateMap map:pm_027 .

map:pom_028 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_028 ;
	rr:predicateMap map:pm_028 .

map:pom_029 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_029 ;
	rr:predicateMap map:pm_029 .

map:pom_030 rdf:type rr:PredicateObjectMap ;
	rr:objectMap map:om_030 ;
	rr:predicateMap map:pm_030 .

map:rules_000 <http://rdfs.org/ns/void#exampleResource> map:map_aircraft_000, map:map_aircraft_001, map:map_geo_000, map:map_operatingairline_000, map:map_publishedairline_000 ;
	rdf:type <http://rdfs.org/ns/void#Dataset> .

map:s_000 rdf:type rr:SubjectMap ;
	rr:template "http://www.example.com/{aircraft_id}" .

map:s_001 rdf:type rr:SubjectMap ;
	rr:template "http://www.example.com/{aircraft_id}" .

map:s_002 rdf:type rr:SubjectMap ;
	rr:template "http://www.example.com/{geo_region}" .

map:s_003 rdf:type rr:SubjectMap ;
	rr:template "http://www.example.com/{operating_airline_iata_code}" .

map:s_004 rdf:type rr:SubjectMap ;
	rr:template "http://www.example.com/{published_airline_iata_code}" .

map:source_000 rml:referenceFormulation ql:CSV ;
	rml:source "sfo-aircraft-tail-numbers-and-models.csv" ;
	rdf:type rml:LogicalSource .

map:source_001 rml:referenceFormulation ql:CSV ;
	rml:source "sfo-air-traffic-landing-statistics.csv" ;
	rdf:type rml:LogicalSource .

map:source_002 rml:referenceFormulation ql:CSV ;
	rml:source "sfo-air-traffic-landing-statistics.csv" ;
	rdf:type rml:LogicalSource .

map:source_003 rml:referenceFormulation ql:CSV ;
	rml:source "sfo-air-traffic-landing-statistics.csv" ;
	rdf:type rml:LogicalSource .

map:source_004 rml:referenceFormulation ql:CSV ;
	rml:source "sfo-air-traffic-landing-statistics.csv" ;
	rdf:type rml:LogicalSource .

